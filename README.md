extractMD
=========

# General

This is useful if you are somewhere where there is no *exiftool* installed.

# Usage

```
extract.py [-h] [-c] INDIR

Extract Metadata

positional arguments:
  INDIR        indir containing files with metadata

optional arguments:
  -h, --help   show this help message and exit
  -c, --check  activate checking for tags; default is deactive

```

